# musicbrainz picard notes

## Options

### File Nameing

#### Rename files when saving

[x] Replace non-ASCII characters

```python
$replace(
$replace(
$replace(
$replace(
$replace(
$replace(
$rreplace(
$rreplace(
$replace(
$lower($if2(%albumartist%,%artist%)/$if($ne(%albumartist%,),%album%/,)$if($gt(%totaldiscs%,1),%discnumber%-,)$if($ne(%albumartist%,),$num(%tracknumber%,2) ,)$if(%_multiartist%,%artist% - ,)%title%)
, ,_)
,_+,_)
,[\,'!],)
,ä,ae)
,ü,ue)
,ö,oe)
,Ä,Ae)
,Ü,Ue)
,Ö,Oe)
```
